call plug#begin()
Plug 'dracula/vim',{'as':'dracula'}
Plug 'preservim/nerdtree'
Plug 'vimwiki/vimwiki'
call plug#end()

nnoremap <C-n> :NERDTreeToggle<CR>
autocmd VimEnter * NERDTree

syntax on
set termguicolors
set number
set noswapfile
set tabstop=4
syntax enable
colorscheme dracula
hi Normal guibg=NONE ctermbg=NONE
