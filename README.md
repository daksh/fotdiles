# Fotdiles
My dotfiles, or well other people's dots hand picked and tweaked to my liking :D

# Note
Somethings which are taken from other people's configs without any changes haven't been provided.

| File Path | Source                                                    |
|-----------|-----------------------------------------------------------|
| ~/.prompt | https://codeberg.org/cobra/.files/src/branch/main/.prompt |
