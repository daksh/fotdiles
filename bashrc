#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source .prompt

alias pmix=pulsemixer
alias q=exit
alias c=clear
alias pman=pacman
alias img=img2sixel

export PATH="$HOME/.local/bin:$PATH"
export TERM='xterm-256color'
export TERMINAL=foot

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '
